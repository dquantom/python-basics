
# Abstract class imports ABC
# abstract method should be empty, no code, no body
# Inherited/sub class should always override the abstract method.

# abstract class example
from abc import ABC, abstractmethod
class Myclass(ABC):
    @abstractmethod
    def calculate(self, x):
        pass # empty body, no code

    def display(self):
        print("In Myclass....")

class Sub1(Myclass):
    def calculate(self, x):
        print(f"Square value = {x*x}")

class Sub2(Myclass):
    def calculate(self, x):
        print(f"Double value = {x*2}")

class Sub3(Myclass):
    def add(self, a, b):
        return a + b

    def calculate(self, x):
        return x*x

s1 = Sub1()
s1.calculate(4)

s2 = Sub2()
s2.calculate(4)


s3 = Sub3()
s3.add(2, 3)
s3.display()
