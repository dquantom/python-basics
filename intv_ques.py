

#1. Write a program to remove duplicates in a string
mystr = 'aabcdedddeeffssighsda'
print(f"My string before {mystr}")
myset = set(mystr)
print(f"My string unique {myset}")

# Do not use set
mydict = dict()
for val in mystr:
    mydict[val] = 1    # any value is okay, because values can be same
print(f"My string unique with dict {''.join(mydict.keys())}")  #use join method to join all chars

# Do not use dictionary
mylist = []
for val in mystr:
    if val not in mylist:
        mylist.append(val)

print(f"My string unique with list {''.join(mylist)}")

# Do not use list / tuple
unstr = ""
for val in mystr:
    if val not in unstr:
        unstr += val

print(f"My string unique with string {unstr}")

# how many times each is repeating.
mydict = dict()
for val in mystr:
    if val in mydict:
        mydict[val] += 1
    else:
        mydict[val] = 1

print(f"My string unique with dictionary with count {mydict}")


#2. Write a program to reverse a string in python
mystr = "quantom"
print(f"The reverse of string with slicing : {mystr[::-1]}")   # slicing
print(f"The reverse of string with rev function : {''.join(reversed(mystr))}")

newstr = ""
for val in mystr:
    newstr = f"{val}{newstr}"
             # q
             # uq
print(f"The reverse with logic : {newstr}")

ln = len(mystr)
newstr = ""
while ln > 0:
    newstr = f"{newstr}{mystr[ln-1]}"
    ln = ln - 1
print(f"The reverse with logic1 : {newstr}")


# qauntom
# mauntoq
# mountaq
# motnuaq
mystr = "quantom"
ln = len(mystr)
ln1 = ln // 2
count = 0
newstr = ""
while count < ln1:
    newstr1 = mystr[ln - 1 - count]
    newstr2 = mystr[count + 1:ln - (count + 1)]
    newstr3 = mystr[count]
    count = count + 1
print(f"The reverse with logic2 : {newstr}")
