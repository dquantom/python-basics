
# a is a variable
# Operators
# NoneType Data Type
a = None
# Numerical Data types : int, float, complex
print(type(a))
a = 7
print(type(a))
a = 7.5
print(type(a))

is_dir = False
print("is dir: ", is_dir)


#f-strings
print(f"Value of a is {a}")

# Type casting or Type conversion
a = 4.5
b = int(a)
print(f"The value of a is {a} and type is {type(a)}")
print(f"The value of b is {b} and type is {type(b)}")

a = 4
b = float(a)
print(f"The value of a is {a} and type is {type(a)}")
print(f"The value of b is {b} and type is {type(b)}")

# Sequence Data types
# str
a = 'vijai sfsadfj; sdf; 233456^^^&&%%$$*IIMNGHU'
print(f"The value of a is {a} and type is {type(a)}")

# Byte
# 0x0 - 0xF
a = 10
b = bytes(a)
print(f"The value of a is {b} and type is {type(b)}")


# list
mylist = [1, 2, 5, 6, 7]
print(f"The value of mylist is {mylist} and type is {type(mylist)}")

mylist = [1, 2, 5.6, 6, 7.3]
print(f"The value of mylist is {mylist} and type is {type(mylist)}")

mylist = [None, "vijai", 5, 6.5, 7]
print(f"The value of mylist is {mylist} and type is {type(mylist)}")
# list type is mutable
a = ["devops", 6, 5.5]
mylist = [None, "vijai", a, 6.5, 3, True]
print(f"The value of mylist is {mylist} and type is {type(mylist)}")
mylist.append(False)
mylist.append(67543)
print(f"The value of mylist is {mylist} and type is {type(mylist)}")

# tuple
# Tuple is immutable
mytuple = (None, "vijai", a, 6.5, 7, True)
print(f"The value of mytuple is {mytuple} and type is {type(mytuple)}")

# Range
for i in range(0, 10, 3):
    print(i)


# Set, it is mutable
myset = {2, 3, 3, 4, 5, 5, 5, 6, 7, 8}
print(f"The value of myset is {myset} and type is {type(myset)}")
myset1 = {456}
myset.update(myset1)
print(f"The value of myset is {myset}")
#frozenset, not mutable
fs = frozenset(myset)
print(f"The value of fs is {fs} and type is {type(fs)}")


# Mapping data types
# dict (dictionary or hash) data type
mydict = {'name': 'vijai', 'id': 1, 456: 456, 'name': 'devops'}
print(f"The value of mydict is {mydict} and type is {type(mydict)}")

mydict = {}
mydict = dict()
mydict = tuple() # set(), frozenset(), list(), tuple()
print(f"The type is {type(mydict)}")

# print(a * 6)  # Multiplication
# print(a / 2)  # Decimal division
# print(a // 2) # Integer division
# print(a - 2)  # Subtraction


