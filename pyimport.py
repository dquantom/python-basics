
def help_me():
    print("Help is on the way...")

class student:
    def __init__(self, name, id, section):
        self.name = name
        self.id = id
        self.section = section

    def display(self):
        print(f"Name of the student is {self.name}")
        print(f"Student's id is {self.id}")
        print(f"Student is studying in {self.section}")

