
# Exceptions in Python

num = 5
for val in range(3):
    try:
        newval = num / val
        print(newval)
    except ZeroDivisionError:
        print("An exception occured, please look in to the error.")
        print("Conintuing operation...")


for val in range(3):
    try:
        newval = num / val
        print(newval)
    except ZeroDivisionError:
        print("An exception occured, please look in to the error.")
        raise ZeroDivisionError("Exception occurred, divison by zero.")
    finally:
        print("Prints anyway.....")

